#ifndef CONVERTED_ACCELERATION_DATA_H
#define CONVERTED_ACCELERATION_DATA_H

struct RawAccelerationData
{
	unsigned char accelX[2];
	unsigned char accelY[2]; 
	unsigned char accelZ[2]; 
};

struct ConvertedAccelerationData
{
	float x;
	float y;
	float z;
	int id;	
};

void conversion(RawAccelerationData*, ConvertedAccelerationData*);

#endif
