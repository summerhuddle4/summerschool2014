#ifndef DATA_TRANSPORT_H
#define DATA_TRANSPORT_H

#include "SendSocket.hpp"
#include "ConvertedAccelerationData.hpp"

class DataTransport
{
	public:
		DataTransport(SendSocket*);
		void send(ConvertedAccelerationData*);
	private:
		SendSocket* socket;
};

#endif
