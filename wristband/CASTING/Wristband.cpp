#include "SendSocket.hpp"
#include "DataTransport.hpp"
#include "ConvertedAccelerationData.hpp"

#include <iostream>

//Arguments are Wristband and Phone IP Address

int main(int argc, char* argv[])
{
	const char * ip_address = argv[1];
	
	SendSocket socket(ip_address); 
	DataTransport gData(&socket);
	
	RawAccelerationData raw;
	ConvertedAccelerationData converted;
		
	// fill raw with data (eventually from accelerometer)
	conversion(&raw, &converted);
	
	gData.send(&converted);

	std::cout << "sent" << std::endl;
	
	return 0;	
}
