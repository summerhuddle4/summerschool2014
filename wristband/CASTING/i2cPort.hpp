#ifndef i2c_PORT_H
#define i2c_PORT_H

#include <cstdio>

class i2cPort
{
	public:
		i2cPort();
		~i2cPort();
		int i2cData;
		virtual bool SetAddress(unsigned char DeviceAddress);
		virtual bool Write(const void * buffer, int length);
		virtual bool Receive(void* buffer, int length);
};

#endif
