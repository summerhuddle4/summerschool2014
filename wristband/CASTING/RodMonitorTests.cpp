#include "RodMonitor.hpp"
#include <iostream>
#include <assert.h>

using namespace std;

void test_rod_monitor_start_starts_recording()
{
	RodMonitor monitor;
	monitor.Start();
	assert(monitor.IsRecording());	
}

void test_rod_monitor_stops_recording()
{
	RodMonitor monitor;
	monitor.Start();
	monitor.Stop();
	assert(monitor.IsRecording() == false);	
}

void test_rod_monitor_calculate_duration()
{
	RodMonitor monitor;
	monitor.Start();
	cout << "Duration: " << monitor.Stop();
	assert(monitor.Stop() == 0);	
}

int main()
{
   test_rod_monitor_start_starts_recording();
   test_rod_monitor_stops_recording();
   test_rod_monitor_calculate_duration();
   cout << "All tests passed!" << endl;   	
}
