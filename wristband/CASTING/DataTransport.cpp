#include "DataTransport.hpp"
#include "SendSocket.hpp"
#include "ConvertedAccelerationData.hpp"

DataTransport::DataTransport(SendSocket* s)
:socket(s)
{
	
}

void DataTransport::send(ConvertedAccelerationData *converted)
{
	socket->SendData(converted, sizeof(*converted));
}



