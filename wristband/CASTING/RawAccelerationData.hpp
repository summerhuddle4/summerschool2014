#ifndef RAW_ACCELERATION_DATA_H
#define RAW_ACCELERATION_DATA_H

struct RawAccelerationData
{
	unsigned char accelX[2];
	unsigned char accelY[2]; 
	unsigned char accelZ[2]; 
};

#endif
