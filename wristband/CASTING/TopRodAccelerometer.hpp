#ifndef TOP_ROD_ACCELEROMETER_H
#define TOP_ROD_ACCELEROMETER_H

#include "RawAccelerationData.hpp"
#include "ConvertedAccelerationData.hpp"

void conversion(RawAccelerationData*, ConvertedAccelerationData*);

#endif
