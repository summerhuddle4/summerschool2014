#include <StdoutLog.h>
#include <SocketHandler.h>
#include "UdpTestSocket.h"


int main(int argc,char *argv[])
{
	SocketHandler h;
	StdoutLog log;
	h.RegStdLog(&log);
	UdpTestSocket s(h);
	port_t port = 5000;

	if (s.Bind(port, 10) == -1)
	{
		printf("Exiting...\n");
		exit(-1);
	}
	else
	{
		printf("Ready to receive on port %d\n",port);
	}
	h.Add(&s);
	h.Select(1,0);
	while (h.GetCount())
	{
		h.Select(1,0);
	}
}
