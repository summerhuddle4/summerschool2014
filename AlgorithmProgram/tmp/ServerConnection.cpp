#include<stdio.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<iostream>



int main ()
{

	int socket_desc; 
	struct sockaddr_in server; 
	char *message;
	
	//create socket	
	socket_desc = socket(AF_INET, SOCK_STREAM, 0); 
	if (socket_desc ==-1)
	{
	
		printf("could not create socket");
	}
	
	server.sin_addr.s_addr = 
	inet_addr("74.125.235.20");
	 
	server.sin_family = AF_INET;
	server.sin_port = htons (80);
	
	//Connect to remote server
	if (connect (socket_desc, (struct sockaddr *)&server, sizeof(server)) <0) 
	{
	
	puts("connect error"); 
	return 1; 
	
	}

	puts("Connected\n"); 
	
	//send some data
	message = "Get / HTTP/1.1\r\n\r\n";
	if (send (socket_desc, message , strlen(message), 0) <0)
	{
		puts("Send Failed"); 
		return 1;
	
	}
	puts ("Data Send\n");
	
	return 0;
}
