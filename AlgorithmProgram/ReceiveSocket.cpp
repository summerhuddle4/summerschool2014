#include "ReceiveSocket.hpp"
#include "PhoneDataTransfer.hpp"
#include <cstdlib>
#include <string.h>
#include <time.h>
#include <sys/time.h>

void ReceiveSocket::diep(const char *s)
{
    perror(s);
    exit(1);
}

ReceiveSocket::ReceiveSocket()
{
   slen=sizeof(si_other);
   
   if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)
     diep("socket");

    memset((char *) &si_me, 0, sizeof(si_me));
    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(2465);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(s, (const sockaddr* )&si_me, sizeof(si_me))==-1)      
		diep("bind");

}

void ReceiveSocket::ReceiveData(void* buffer, size_t length) 
{	
	if (recvfrom(s, buffer, length, 0, (sockaddr*)&si_other,&slen)==-1)
	{	
		diep("recvfrom()");
	}
	
	printf("Received packet from %s:%d, size=%d\n", 
	   inet_ntoa(si_other.sin_addr), 
	   ntohs(si_other.sin_port),
	   length);
	   
	char *charBuffer = (char*) buffer;
	for (int i=0; i<length; i++)
	   printf("%c", charBuffer[i]);
	   
    printf("\n");	   

}

ReceiveSocket::~ReceiveSocket()
{
   close(s);
}


