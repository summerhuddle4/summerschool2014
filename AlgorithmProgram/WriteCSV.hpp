#ifndef WRITE_CSV_H
#define WRITE_CSV_H
#include "SpeedData.csv"
#include "VelocitySpeedConverter.hpp"
#include <stdio.h>



class WriteCSV

{
	public: 
		
	void WriteFile(double &speed); 
	
	};
	
void WriteCSV::WriteFile(double &speed)
{
	
	FILE *f = fopen("SpeedData.csv", "w"); 		
	fprintf(f, "%f", speed);
	fclose(f); 
	
}
	
	
	
	
#endif	
