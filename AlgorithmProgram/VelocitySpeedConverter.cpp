
#include "VelocitySpeedConverter.hpp"
#include "PhoneDataTransfer.hpp"
#include "ReceiveSocket.hpp"
#include <time.h>
#include <cmath>
#include <iostream>
#include <stdio.h>
#include <sys/time.h>
using namespace std; 


struct velocity
{
	float x;
	float y;
	float z;
};
	
struct PreviousVelocity
{	
	float x;
	float y;
	float z;
};
	
void VelocitySpeedConverter::Convert(convertedAccel & accel)
{	 
	
	velocity vel; 
	PreviousVelocity PrevVel;
	
	 
	for (int i = 0; i < NPACK; i++) 
	{	
		vel.x = PrevVel.x + (accel.x * 0.000784532); // x * 0.008 * 9.80665 * 0.01
		vel.y = PrevVel.y + (accel.y * 0.000784532);
		vel.z = PrevVel.z + (accel.z * 0.000784532);
		
		speed = sqrt(pow(vel.x, 2) + pow(vel.y, 2) + pow(vel.z, 2));
						
		FILE *f= fopen("SpeedData.csv","a"); 		 		
	    fprintf(f, "%f;", speed); 
		
		
		PrevVel.x = vel.x;
		PrevVel.y = vel.y;
		PrevVel.z = vel.z;	
		
		
		fclose(f);	
	} 	
		
		
	
	
}	

