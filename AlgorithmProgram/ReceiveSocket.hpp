#ifndef RECEIVE_SOCKET_H
#define RECEIVE_SOCKET_H

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#define BUFLEN 512
#define NPACK 1

class ReceiveSocket
{
	public:
	
	ReceiveSocket(); 
	~ReceiveSocket();
	void ReceiveData(void* buffer, size_t length);
	void diep(const char *s);
	 	
	private: 
	
	struct sockaddr_in si_me, si_other;		
    int s,i;
    unsigned int slen;
    
};

#endif
