<?php

	header('Content-Type: application/json');

	exec('pgrep Algorithms | xargs kill -15');

	$file = "SpeedData.csv";
	$json['label'] = "Your Cast";

	if ($handle = fopen($file, "r")) {
		$contents = fread($handle, filesize($file));
//exit($contents);
		$plots = explode(';', $contents);
		$time = 0;
		foreach ($plots AS $p)
		{
			
			//list($time, $speed) = explode(',', trim($p));
			if ($p != NULL)
			{
				$json['data'][][] = $time . ',' . $p;
				$time += 1;
			}
		}
	}

	$newData = json_encode($json);
	$newData = str_replace("[\"", "[", $newData);
	$newData = str_replace("\"]", "]", $newData);
	echo $newData;
?>
